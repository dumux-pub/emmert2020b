# Emmert2020b

Welcome to the dumux pub module for Emmert2020b
======================================================

This module contains the source code for the examples in the
paper

__S. Emmert et al. (2020)__, _[The Role of Retardation, Attachment and Detachment Processes during Microbial Coal-Bed Methane Production after Organic Amendment](https://doi.org/10.3390/w12113008)_ (Water) 

``` 
@article{emmert2020role,
  title={The role of retardation, attachment and detachment processes during microbial coal-bed methane production after organic amendment},
  author={Emmert, Simon and Davis, Katherine and Gerlach, Robin and Class, Holger},
  journal={Water},
  volume={12},
  number={11},
  pages={3008},
  year={2020},
  doi = {https://doi.org/10.3390/w12113008},
  publisher={Multidisciplinary Digital Publishing Institute}
}
```

For building from source create a folder and download `installemmert2020b.sh` from this repository.
```bash
chmod +x installemmert2020b.sh
./installemmert2020b.sh
```
will download configure and compile all dune dependencies. Furthermore you need to have the following basic requirement installed

* C, C++ compiler (C++14 required)
* Fortran compiler (gfortran)
* UMFPack from SuiteSparse



## Dependencies on other DUNE modules

| module | branch | commit hash |
|:-------|:-------|:------------|
| dune-common | releases/2.7 | 20a8172d68a26d3f87457c3e2f2a7a7e24f609db |
| dune-geometry | releases/2.7 | fb75493b000410cbd157927f80c68ec9e86c5553 |
| dune-grid | releases/2.7 | 550c58dc9c75ebf9ff2d640bd5ccbfd100714ffe |
| dune-localfunctions | releases/2.7 | ad588f9599b12b1615e760ae0106594f2059ab3e |
| dune-istl | releases/2.7 | 399ed5996e0b15c2038290810ad0f07d5f1e4c30 |
| dune-foamgrid | releases/2.7 | d49187be4940227c945ced02f8457ccc9d47536a |
| dumux | releases/3.2 | 398f8fb841e1ace6132f265b482df1147ad46b1b |

The examples are in the folder
```
emmert2020b/build-cmake/appl/mecbm/column/implicit/ 
emmert2020b/build-cmake/appl/mecbm/column/implicit/retardation/ 
emmert2020b/build-cmake/appl/mecbm/column/implicit/bioretardation/
```

The examples are compiled by the following commands in their respective subfolders

```bash
make test_mecbmcolumn_tpfa
make test_mecbmretardationcolumn_tpfa
make test_mecbmbioretardationcolumn_tpfa
```

Run an example (e.g. Case11 Coal++ is identified with Coal11)
```bash
cd emmert2020b/build-cmake/appl/mecbm/column/implicit/
./test_mecbmcolumn_tpfa test_mecbmcolumnCoal11LessBiofilm.input
```


Installation with Docker
========================

Create a new folder in your favourite location and change into it

```bash
mkdir Emmert2020b
cd Emmert2020b
```

Download the container startup script

[docker_emmert2020b.sh](https://git.iws.uni-stuttgart.de/dumux-pub/emmert2020b/-/blob/master/docker_emmert2020b.sh)


Open the Docker Container
```bash
bash docker_emmert2020b.sh open
```

After the script has run successfully, you may build all executables

```bash
cd emmert2020b/build-cmake
make build_tests
```

and you can run them individually. They are located in the build-cmake folder according to the following paths:

- appl/mecbm/column/implicit
- appl/mecbm/column/implicit/retardation
- appl/mecbm/column/implicit/bioretardation

They can be executed with an input e.g., by running

```bash
cd appl/mecbm/column/implicit
./test_mecbmcolumn_tpfa test_mecbmcolumnCoal11LessBiofilm.input
```
